//Express JS

// npm -v --> in Git bash, checks the version of npm
// node -v --> in Git bash, checks the version of nodeJS
//npm init --> used to setup new or existing npm package, press enter multiple times until "Is this OK?"
// npm install express --> to install Express in the directory and save it in dependencies list (creates lockfile e.g. package-lock.json)
// nodemon index --> in Git bash; wraps your Node app, watches the file system, and automatically restarts the process


// First, we load the expressjs module into our application and save it in a variable called express.


const express = require("express");
	//Create an application with expressjs
		//This creates an application that uses express and stores in a variable called app, which makes it easier to use expressjs methods in our api

const app = express();
	// port is just a variable that contain the port number we want to designate for our new expressjs api

const port = 4000;

	// To create a new route in expressjs, we first access from our express() package, our method. For get method request, access express (app), and use get()
		// Syntax:
		// app.routemethod('/',(req,res)=>{
		// 		function to handle request and response
		// })

app.use(express.json())
	//app.use(express.json()) --> allows app to read JSON data, such as the one entered in postman's body
	//setup for allowing the server to handle data from requests

app.get('/',(req,res)=>{

	// res.send() ends the response and sends your response to the client
	// res.send() already does the res.writeHead() and res.end() automatically
	res.send("Hello world from our new ExpressJS API")

})

/*
	Mini activity:
		Create a get route in expressJS which will be able to send a message.
		endpoint: hello

		Message: Hello, batch 157
*/

app.get('/hello',(req,res)=>{
		res.send("Hello, batch 157")

})

/*app.post('/hello', (req, res) => {
	console.log(req.body)

	// req.body --> requests from body tab of postman
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}`)
})*/

/*
	Mini activity:
		Change the message that we send "Hello, I am <name>, I am <age>. I live in <address>.""
*/

app.post('/hello', (req, res) => {
	console.log(req.body)

	// req.body --> requests from body tab of postman
	res.send(`Hello, I am ${req.body.name}, I am ${req.body.age} years old. I live in ${req.body.address}.`)
})

let users = [];

app.post('/signup', (req,res) => {
	console.log(req.body)

	if(req.body.username != '' && req.body.password != '') {
		users.push(req.body);

		res.send(`User ${req.body.username} successfully registered.`)
	}

	else{
		res.send("Please input both username and password.")
	}
})

app.put('/change-password', (req,res) => {

	let message;

	for(let i = 0; i < users.length; i++) {

		if(req.body.username === users[i].username) {
			users[i].password = req.body.password;
			message = `User ${req.body.username}'s password has been updated.`

			break;
		}

		else {
			message = `User does not exist`
		}
	}

	res.send(message);

})

app.get('/home',(req,res)=>{
		res.send("Welcome to home page")

})

app.get('/users',(req,res)=>{
		res.send(users)

})

app.delete('/delete-user',(req,res)=>{
	console.log(req.body)

	if(req.body.username != '' && req.body.password != '') {
		users.pop(req.body);
		res.send(`User ${req.body.username} has been deleted.`)
	}

	else {
		res.send("There is no user account to delete.")
	}

})

	// app.listen() allows us to designate the correct port to our new expressjs api and once the server is running we can then run a function that runs a console.log() which contains a message that the server is running

app.listen(port, ()=>console.log(`Server is running at port ${port}`));
	

 